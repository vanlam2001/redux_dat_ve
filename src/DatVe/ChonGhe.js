import React, { Component } from 'react'
import { connect } from 'react-redux'
import { formatter } from './VND'
class ChonGhe extends Component {
    renderTongTien = () => {
        let tongTien = 0;
        this.props.chonGhe.map(ghe => {
            return tongTien = tongTien + ghe.gia;
        })
        return formatter.format(tongTien);
    }

    render() {
        return (
            <div>
                <div className="container p-5">
                    <h2 className='text-success'>Danh sách đặt ghế</h2>
                    <div style={{ display: 'flex', alignItems: 'center' }} className='mb-3'>
                        <span style={{ width: '30px', height: '30px', display: 'inline-block' }} className='bg-light mr-2'></span>
                        <span style={{ fontWeight: "bolder" }} className='text-light'>Ghế chưa đặt</span>
                    </div>

                    <div style={{ display: 'flex', alignItems: 'center' }} className='mb-3'>
                        <span style={{ width: '30px', height: '30px', display: 'inline-block' }} className='bg-danger mr-2'></span>
                        <span style={{ fontWeight: "bolder" }} className='text-danger'>Ghế đã đặt</span>
                    </div>

                    <div style={{ display: 'flex', alignItems: 'center' }} className='mb-5'>
                        <span style={{ width: '30px', height: '30px', display: 'inline-block' }} className='bg-success mr-2'></span>
                        <span style={{ fontWeight: "bolder" }} className='text-success'>Ghế đang chọn</span>
                    </div>
                </div>

                <table className='table table-dark table-inverse text-white'>
                    <thead className='thead-inverse'>
                        <tr>
                            <th>Số ghế</th>
                            <th>Giá</th>
                            <th>Action</th>\

                        </tr>
                    </thead>
                    <tbody>
                        {this.props.chonGhe.map((ghe) => {

                            return (
                                <tr>
                                    <td>{ghe.soGhe}</td>
                                    <td>{formatter.format(ghe.gia)}</td>
                                    <td><button onClick={() => {
                                        this.props.chucNangXoa(ghe.soGhe)
                                    }} className='btn btn-danger'>Xóa</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                    <tr>
                        <td>Tổng tiền</td>
                        <td>{this.renderTongTien()}</td>
                    </tr>
                </table>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return { chonGhe: state.gheReducer.chonGhe }
}

let mapDispatchToProps = (dispatch) => {
    return {
        chucNangXoa: (soGhe) => {
            let action = {
                type: 'XOA_GHE',
                soGhe
            }

            dispatch(action);
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChonGhe);
