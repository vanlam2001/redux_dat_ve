import { combineReducers } from "redux";
import { gheReducer } from "./gheReducer";

export const rootReducer = combineReducers({
    gheReducer
})