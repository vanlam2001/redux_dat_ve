export let stateGhe = (state) => {
    if (state == false) {
        return 'btn btn-light btn-sm mr-4'
    }

    else if (state == true) {
        return 'btn btn-danger btn-sm mr-4'
    }

    else {
        return 'btn btn-success btn-sm mr-4'
    }
}